<?php $this_page_title = get_term_by('slug',$term,$taxonomy)->name; ?>
<?php
  $term_slug = get_query_var('productscategory');
  $term_info = get_term_by('slug',$term_slug,'productscategory');
  $this_term_name = $term_info->name;
  $this_term_slug = $term_info->slug;
  $this_term_id = $term_info->term_id;
?>
<?php get_header(); ?>

content

<?php get_footer(); ?>