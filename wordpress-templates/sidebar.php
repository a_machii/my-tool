<?php $myptype = $post->post_type; ?>
        <div id="side">
          <div class="pconly" id="cal">
<?php get_cpt_calendar('news'); ?>
          </div><!--/cal-->

          <!-- 新着を表示 -->
          <div class="slnav pconly blognav">
            <p><img src="<?php echo home_url('/'); ?>common/img/blog/snew_ttl.gif" width="190" height="38" alt="新着情報"></p>
            <ul>
<?php
  $args = array (
    'posts_per_page' => 5,
    'post_type' => 'news',
  );
  $my_posts = get_posts( $args ); // $posts = とは書かない
  global $post; // テンプレートファイル内なら書かなくても良い
  $count_post = 0;
  foreach ( $my_posts as $post ) : //$postにしないとthe_titleが取得できない
  setup_postdata( $post );
?>

              <li<?php if($count_post==0) : ?> class="top"<?php endif; ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php
  $count_post++;
  endforeach;
  wp_reset_postdata();
?>
            </ul>
          </div><!--/slnav-->

          <!-- カテゴリを表示 pc -->
          <div class="slnav pconly blognav">
                  <p><img src="<?php echo home_url('/'); ?>common/img/blog/scat_ttl.gif" width="190" height="38" alt="カテゴリ"></p>
<?php
  // カスタム分類名
  $taxonomy = 'newscategory';
  // パラメータ
  $args = array(
    // 子タームの投稿数を親タームに含める
    'pad_counts' => true,
    // 投稿記事がないタームも取得
    'hide_empty' => false
  );
  // カスタム分類のタームのリストを取得
  $terms = get_terms( $taxonomy , $args );
  if ( count( $terms ) != 0 ) {
    echo '<ul>';
    $count_post = 0;
    // タームのリスト $terms を $term に格納してループ
    foreach ( $terms as $term ) {
      // タームのURLを取得
      $term = sanitize_term( $term, $taxonomy );
      $term_link = get_term_link( $term, $taxonomy );
      if ( is_wp_error( $term_link ) ) {
        continue;
      }

      // 子タームの場合はCSSクラス付与
      if( $term->parent != 0 ) {
        echo '<li class="children">';
      } else {
        if($count_post == 0) {
          echo '<li class="top">';
        }else{
          echo '<li>';
        }
      }

      // タームのURLと名称を出力
      echo '<a href="' . esc_url( $term_link ) . '">' . $term->name . '</a>';
      echo '</li>';
     $count_post++;
    }
    echo '</ul>';
  }
?>
          </div><!--/slnav-->

          <!-- カテゴリを表示 rsp -->
          <div class="slnav rsponly blognav">
            <p><img src="<?php echo home_url('/'); ?>common/img/blog/scat_ttl.gif" width="190" height="38" alt="カテゴリ"></p>
<?php
// カスタム分類名
$taxonomy = 'newscategory';

// パラメータ
$args = array(
  // 子タームの投稿数を親タームに含める
  'pad_counts' => true,
  // 投稿記事がないタームも取得
  'hide_empty' => false
);

// カスタム分類のタームのリストを取得
$terms = get_terms( $taxonomy , $args );

if ( count( $terms ) != 0 ) {
  echo '<ul>';
  echo '<li class="top">';
  echo '<select  onChange="document.location.href=this.options[this.selectedIndex].value;">';
  $count_post = 0;
  // タームのリスト $terms を $term に格納してループ
  foreach ( $terms as $term ) {
    // タームのURLを取得
    $term = sanitize_term( $term, $taxonomy );
    $term_link = get_term_link( $term, $taxonomy );
    if ( is_wp_error( $term_link ) ) {
      continue;
    }

    // タームのURLと名称を出力
    echo '<option value="'. home_url("/").'blog/blogcategory/'. $term->slug. '/">'. $term->name. '</option>';
    $count_post++;
  }
  echo '</select>';
  echo '</li>';
  echo '</ul>';
}
?>
          </div><!--/slnav-->

          <div class="slnav blognav blogarc">
            <p><img src="<?php echo home_url('/'); ?>common/img/blog/sarc_ttl.gif" width="190" height="38" alt="カテゴリ"></p>
            <ul>
              <li class="top">
                <select name="archive-dropdown" onChange='document.location.href=this.options[this.selectedIndex].value;'>
                  <option value=""><?php echo attribute_escape(__('Select Month')); ?></option>
                  <?php wp_get_archives( 'post_type=blog&type=monthly&format=option&show_post_count=1' ); ?>
                </select>
              </li>
            </ul>
          </div><!--/slnav-->
        </div><!--/side-->