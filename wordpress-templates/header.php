<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
<?php
  global $this_page_title;
  global $this_page_keywd;
  global $this_page_desc;
  global $this_page_slug;
?>
  <meta charset="<?php bloginfo('charset'); ?>">
  <title><?php if($this_pagetitle) : echo $this_pagetitle. ' | '; endif; ?>ページタイトル</title>
  <meta name="description" content="<?php if($this_page_desc) : echo $this_page_desc. ' - '; elseif($this_pagetitle) : echo $this_pagetitle. ' - '; endif; ?>サイトの説明" >
  <meta name="keywords" content="<?php if($this_page_keywd) : echo $this_page_keywd. ','; elseif($this_pagetitle) : echo $this_pagetitle. ','; endif; ?>キーワード" >
  <meta property="og:url" content="<?php echo home_url('/'); ?><?php if($this_page_slug) : echo $this_page_slug; elseif($this_archive_title) : echo $this_archive_title; endif; ?>">
<?php wp_head(); ?>
</head>